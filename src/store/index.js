import api from '../../api/products.js';
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    cart: {},
    total: 0,
    orderList: []
  },
  mutations: {
    updateProducts(state, products) {
      state.products = products;
    },
    updateCard(state, data) {
      state.cart = {
        ...state.cart,
        [data.id]: !state.cart[data.id] ? data.amount : state.cart[data.id] + data.amount
      }
      state.orderList.unshift(`${data.id} - ${data.amount} кг`);
    },
    updateTotal(state) {
      let total = 0;
      Object.keys(state.cart).forEach(key => {
        total = total + state.cart[key] * state.products.find(product => product.id === key).price;
      });
      state.total = +total.toFixed(2);
    }
  },
  getters: {
    allProducts(state) {
      return state.products;
    },
    productsTotal(state) {
      return state.total;
    },
    ordersList(state) {
      return state.orderList;
    }
  },
  actions: {
    async getProducts(ctx) {
      const products = await api.getProductsList();
      ctx.commit('updateProducts', products);
    }
  }
})
